import 'package:flutter/material.dart';
import 'package:tablecalcus/table_cal/table_calendar.dart';

class CalPage extends StatefulWidget {
  @override
  _CalPageState createState() => _CalPageState();
}

class _CalPageState extends State<CalPage> {
  CalendarController _calendarController = CalendarController();


  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Scaffold(
        body: Column(
          children: <Widget>[
            TableCalendar(calendarController: _calendarController),
          ],
        ),
      ),
    );
  }
}