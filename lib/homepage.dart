import 'package:flutter/material.dart';
import 'package:tablecalcus/table_cal/table_calendar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  CalendarController _calendarController = CalendarController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: _buildPage(),
      ),
    );
  }

  _buildPage() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
          child: TableCalendar(
            calendarController: _calendarController,
            availableGestures: AvailableGestures.horizontalSwipe,
            headerStyle: HeaderStyle(
                headerMargin: EdgeInsets.only(bottom: 25.0),
                centerHeaderTitle: true,
                formatButtonVisible: false,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(width: 2.0, color: Colors.grey))),
            startingDayOfWeek: StartingDayOfWeek.monday,
          ),
        ),
        RaisedButton(
          onPressed: () {
            print("Test");
            _calendarController.setSelectedDay(DateTime.parse('2000-01-02'));
            _calendarController.setFocusedDay(DateTime.parse('2000-01-02'));
          },
          child: Text('Test'),
        )
      ],
    );
  }
}
