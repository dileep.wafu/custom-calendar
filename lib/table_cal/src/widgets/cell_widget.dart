//  Copyright (c) 2019 Aleksander Woźniak
//  Licensed under Apache License v2.0

part of table_calendar;

class _CellWidget extends StatelessWidget {
  final String text;
  final bool isUnavailable;
  final bool isSelected;
  final bool isToday;
  final bool isWeekend;
  final bool isOutsideMonth;
  final bool isHoliday;
  final CalendarStyle calendarStyle;

  const _CellWidget({
    Key key,
    @required this.text,
    this.isUnavailable = false,
    this.isSelected = false,
    this.isToday = false,
    this.isWeekend = false,
    this.isOutsideMonth = false,
    this.isHoliday = false,
    @required this.calendarStyle,
  })  : assert(text != null),
        assert(calendarStyle != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 250),
      decoration: _buildCellDecoration(),
      margin: const EdgeInsets.all(6.0),
      alignment: Alignment.center,
      child: Text(
        text,
        style: _buildCellTextStyle(),
      ),
    );
  }

  /// Original one

  // Decoration _buildCellDecoration() {
  //   if (isSelected &&
  //       calendarStyle.renderSelectedFirst &&
  //       calendarStyle.highlightSelected) {
  //     return BoxDecoration(
  //         shape: BoxShape.circle, color: calendarStyle.selectedColor);
  //   } else if (isToday && calendarStyle.highlightToday) {
  //     return BoxDecoration(
  //         shape: BoxShape.circle, color: calendarStyle.todayColor);
  //   } else if (isSelected && calendarStyle.highlightSelected) {
  //     return BoxDecoration(
  //         shape: BoxShape.circle, color: calendarStyle.selectedColor);
  //   } else {
  //     return BoxDecoration(shape: BoxShape.circle);
  //   }
  // }

  /// Manipulated one ##################################
  Decoration _buildCellDecoration() {
    if (isSelected &&
        calendarStyle.renderSelectedFirst &&
        calendarStyle.highlightSelected) {
      return BoxDecoration(
        border: Border.all(color: Colors.blue, width: 1.0),
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.grey,);

    } else if (isToday && calendarStyle.highlightToday) {
      return BoxDecoration(
        border: Border.all(color: Colors.blue, width: 1.0),
        borderRadius: BorderRadius.circular(20.0),
        color: Colors.blue);

    } else if (isSelected && calendarStyle.highlightSelected) {
      return BoxDecoration(
        border: Border.all(color: Colors.blue, width: 1.0),
        borderRadius: BorderRadius.circular(20.0),
        color: calendarStyle.selectedColor
        );




    } else if(isOutsideMonth){

      //// ADDED 
      return null;
    }
    
    
    
     else {
      return BoxDecoration(
        border: Border.all(color: Colors.grey, width: 1.0),
        borderRadius: BorderRadius.circular(20.0),
      );
    }
  }

// isOutsideMonth
  TextStyle _buildCellTextStyle() {
    if (isUnavailable) {
      return calendarStyle.unavailableStyle;
    } else if (isSelected &&
        calendarStyle.renderSelectedFirst &&
        calendarStyle.highlightSelected) {
      return calendarStyle.selectedStyle;
    } else if (isToday && calendarStyle.highlightToday) {
      return calendarStyle.todayStyle;
    } else if (isSelected && calendarStyle.highlightSelected) {
      return calendarStyle.selectedStyle;
    } else if (isOutsideMonth && isHoliday) {
      ///#####################################################

      return TextStyle(
        color: Colors.grey[100]
      );
    } else if (isHoliday) {
      return calendarStyle.holidayStyle;
    } else if (isOutsideMonth && isWeekend) {
      ///#####################################################
      return TextStyle(
        color: Colors.grey[100]
      );
    } else if (isOutsideMonth) {
      ///#####################################################

      return TextStyle(
        color: Colors.grey[100]
      );
    } else if (isWeekend) {
      return calendarStyle.weekendStyle;
    } else {
      return calendarStyle.weekdayStyle;
    }
  }
}
